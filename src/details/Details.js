import axios from "axios";
import React from "react";
import { findRenderedComponentWithType } from "react-dom/cjs/react-dom-test-utils.production.min";
import { useLocation, useNavigate } from "react-router-dom";

import './Details.css'

function Details() {
  const location = useLocation()
  const navigate = useNavigate()

  const [itemData, setItemData] = React.useState(null)
  const [imgSrc, setImgSrc] = React.useState(null)
  const [loading, setLoading] = React.useState(true)

  React.useEffect(() => {
    // INITIALIZE ITEM DATA
    if (location.state && location.state.item) {
      setItemData(location.state.item)
      setImgSrc(location.state.item.sprites.front_default)
      setLoading(false)
    } else {
      axios.get(`https://pokeapi.co/api/v2/pokemon${location.pathname}`).then(res => {
        setItemData(res.data)
        setImgSrc(res.data.sprites.front_default)
      })
      .catch((err) => console.log('Error:', err))
      .finally(() => setLoading(false))
    }
  }, [])

  function renderType(type) {
    return (
      <span key={type.type.name} class={`badge badge-pill badge-primary badge-${type.type.name}`}>{type.type.name.toUpperCase()}</span>
    )
  }
  
  return (
    <div className="dets-container">
      <div className="dets-topbar">
        <button type="button" class="btn btn-light" onClick={() => navigate(-1)}>
          <span class="material-icons">arrow_back</span>
        </button>
      </div>
      {
        loading && <div className="dets-spinner">
          <div className="spinner-border" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        </div>
      }
      {
        itemData && <div>
          <p className="font-weight-bold">{itemData.name.toUpperCase().replace('-', ' ')}</p>

          <div className="type-container">
            {itemData.types.map(type => renderType(type))}
          </div>

          <div className="sprites-container">
            <img src={imgSrc} />
            <div className="sprites-button-container">
              <button type="button" class="btn btn-primary" onClick={() => setImgSrc(itemData.sprites.front_default)}>Default</button>
              {itemData.sprites.front_female && <button type="button" class="btn btn-danger" onClick={() => setImgSrc(itemData.sprites.front_female)}>Female</button>}
              {itemData.sprites.front_shiny && <button type="button" class="btn btn-primary" onClick={() => setImgSrc(itemData.sprites.front_shiny)}>Shiny</button>}
              {itemData.sprites.front_shiny_female && <button type="button" class="btn btn-danger" onClick={() => setImgSrc(itemData.sprites.front_shiny_female)}>Shiny Female</button>}
            </div>
          </div>

          <div className="stats-container">
            <p className="font-weight-bold text-center">STATS</p>
            <div className="stats-row">
              <div className="stats-left">
                {itemData.stats.map(stat => <div className="stats-col">
                  <p key={stat.stat.name}>{stat.stat.name.toUpperCase().replace('-', ' ')}</p>
                </div>
                )}
              </div>
              <div className="stats-right">
                {itemData.stats.map(stat => <div className="stats-col font-weight-bolder">
                  <p key={stat.stat.name}>{stat.base_stat}</p>
                </div>
                )}
                <div className="stats-col">
                </div>
              </div>
              {/* {itemData.stats.map(stat => <p key={stat.stat.name}>{stat.stat.name.toUpperCase().replace('-', ' ')} {stat.base_stat}</p>)} */}
            </div>
          </div>
        </div>
      }
      
    </div>
  )
}

export default Details;