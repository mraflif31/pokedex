import {
  Routes,
  Route
} from "react-router-dom";

// import './App.css';
import Home from './home/Home.js'
import Details from './details/Details.js'

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/:name" element={<Details />} />
        <Route path="/" element={<Home />} />
      </Routes>
    </div>
  );
}

export default App;
