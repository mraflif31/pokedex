import React from 'react';
import axios from 'axios'
import InfiniteScroll from 'react-infinite-scroll-component';

import { useNavigate  } from "react-router-dom";

import './Home.css'

const limit = 30

function Home() {
  const navigate = useNavigate ();
  
  const [items, setItems] = React.useState([])
  const [itemsData, setItemsData] = React.useState([])
  const [hasMore, setHasMore] = React.useState(true)
  const [count, setCount] = React.useState(null)

  const [typeFilter, setTypeFilter] = React.useState([])
  const [isFiltering, setFiltering] = React.useState(false)

  const [display, setDisplay] = React.useState(null)
  const [loading, setLoading] = React.useState(true)

  // INITIALIZE DATA
  React.useEffect(() => {
    axios.get(`https://pokeapi.co/api/v2/pokemon/?offset=0&limit=${limit}`).then(res => {
      setItems([...items, ...res.data.results])
      setCount(res.data.count)

      Promise.all(res.data.results.map(item => axios.get(item.url))).then(result => {
        setItemsData([...itemsData, ...result.map(item => item.data)])
      })
      .catch((err) => console.log('Error:', err))
      .finally(() => {
        setLoading(false)
      })
    })
    .catch((err) => console.log('Error:', err))
  }, [])

  // IF HEIGHT IS SMALLER
  React.useEffect(() => {
    const root = document.getElementById('scrollableDiv');
    if (!loading && hasMore && root.scrollHeight <= root.clientHeight) {
      getMoreData();
    }
  }, [loading, hasMore, isFiltering]);

  function getMoreData() {
    if (items && count && items.length >= count) {
      setHasMore(false)
      return;
    }
    
    setLoading(true)
    axios.get(`https://pokeapi.co/api/v2/pokemon/?offset=${items.length}&limit=${limit}`).then(res => {
      setItems([...items, ...res.data.results])
      Promise.all(res.data.results.map(item => axios.get(item.url))).then(result => {
        setItemsData([...itemsData, ...result.map(item => item.data)])
        setLoading(false)
      })
      .catch((err) => console.log('Error:', err))
    })
    .catch((err) => console.log('Error:', err))
  }

  function toDetails(item) {
    navigate(`/${item.name}`, { state: {item: item} })
  }

  function filterItem(item) {
    if (!isFiltering) return true
    if (typeFilter.length > 0) {
      let i = 0
      while (i < typeFilter.length) {
        let j = 0
        while (j < item.types.length) {
          if (typeFilter[i] === item.types[j].type.name) return true
          j++
        }
        i++
      }
    } else {
      return true
    }
    return false
  }

  function clearFilter() {
    document.getElementById("inlineCheckbox1").checked = false;
    document.getElementById("inlineCheckbox2").checked = false;
    document.getElementById("inlineCheckbox3").checked = false;
    document.getElementById("inlineCheckbox4").checked = false;
    document.getElementById("inlineCheckbox5").checked = false;
    document.getElementById("inlineCheckbox6").checked = false;
    document.getElementById("inlineCheckbox7").checked = false;
    document.getElementById("inlineCheckbox8").checked = false;
    document.getElementById("inlineCheckbox9").checked = false;
    document.getElementById("inlineCheckbox10").checked = false;
    document.getElementById("inlineCheckbox11").checked = false;
    document.getElementById("inlineCheckbox12").checked = false;
    document.getElementById("inlineCheckbox13").checked = false;
    document.getElementById("inlineCheckbox14").checked = false;
    document.getElementById("inlineCheckbox15").checked = false;
    document.getElementById("inlineCheckbox16").checked = false;
    document.getElementById("inlineCheckbox17").checked = false;
    document.getElementById("inlineCheckbox18").checked = false;
    setTypeFilter([])
    setFiltering(false)
  }

  function check(type) {
    let index = typeFilter.indexOf(type)
    if (index > -1) {
      let temp = Array(typeFilter)
      temp.splice(index, 1)
      setTypeFilter(temp)
    } else {
      setTypeFilter([...typeFilter, type])
    }
  }

  function renderType(type) {
    return (
      <span key={type.type.name} class={`badge badge-pill badge-primary badge-${type.type.name}`}>{type.type.name.toUpperCase()}</span>
    )
  }

  return (
    <div className='home-container'>
      <div className='home-title'>
        <h2>PokeX</h2>
      </div>
      {/* <span>{display}</span> */}
      <div className='scroll-container'>
        <div
          id="scrollableDiv"
          style={{
            height: '300px',
            width: '400px',
            overflow: 'auto',
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          <InfiniteScroll
            id="infScroll"
            dataLength={itemsData.length}
            next={getMoreData}
            style={{ display: 'flex', flexDirection: 'column' }}
            hasMore={hasMore}
            loader={<h4>Loading...</h4>}
            scrollableTarget="scrollableDiv"
          >
            {itemsData.map(item => <div key={item.name} onClick={() => toDetails(item)} onMouseOver={() => setDisplay(item)} className='item' style={{ display: filterItem(item) ? 'flex' : 'none' }}>
              <span className='item-number'>#{String(item.id).padStart(5, '0')}</span>
              <span className='item-name font-weight-bold'>{item.name.toUpperCase().replace('-', ' ')}</span>
              <div className="type-container">
                {item.types.map(type => renderType(type))}
              </div>
            </div>)          }
          </InfiniteScroll>
        </div>
        <div>
          <div className="form-check">
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="checkbox" id="inlineCheckbox1" disabled={isFiltering} onChange={() => check('bug')}/>
              <label className="form-check-label" for="inlineCheckbox1">Bug</label>
            </div>
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="checkbox" id="inlineCheckbox2" disabled={isFiltering} onChange={() => check('dark')}/>
              <label className="form-check-label" for="inlineCheckbox2">Dark</label>
            </div>
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="checkbox" id="inlineCheckbox3" disabled={isFiltering} onChange={() => check('dragon')}/>
              <label className="form-check-label" for="inlineCheckbox3">Dragon</label>
            </div>
          </div>
          <div className="form-check">
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="checkbox" id="inlineCheckbox4" disabled={isFiltering} onChange={() => check('electric')}/>
              <label className="form-check-label" for="inlineCheckbox4">Electric</label>
            </div>
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="checkbox" id="inlineCheckbox5" disabled={isFiltering} onChange={() => check('fairy')}/>
              <label className="form-check-label" for="inlineCheckbox5">Fairy</label>
            </div>
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="checkbox" id="inlineCheckbox6" disabled={isFiltering} onChange={() => check('lighting')}/>
              <label className="form-check-label" for="inlineCheckbox6">Fighting</label>
            </div>
          </div>
          <div className="form-check">
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="checkbox" id="inlineCheckbox7" disabled={isFiltering} onChange={() => check('fire')}/>
              <label className="form-check-label" for="inlineCheckbox7">Fire</label>
            </div>
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="checkbox" id="inlineCheckbox8" disabled={isFiltering} onChange={() => check('flying')}/>
              <label className="form-check-label" for="inlineCheckbox8">Flying</label>
            </div>
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="checkbox" id="inlineCheckbox9" disabled={isFiltering} onChange={() => check('ghost')}/>
              <label className="form-check-label" for="inlineCheckbox9">Ghost</label>
            </div>
          </div>
          <div className="form-check">
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="checkbox" id="inlineCheckbox10" disabled={isFiltering} onChange={() => check('grass')}/>
              <label className="form-check-label" for="inlineCheckbox10">Grass</label>
            </div>
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="checkbox" id="inlineCheckbox11" disabled={isFiltering} onChange={() => check('ground')}/>
              <label className="form-check-label" for="inlineCheckbox11">Ground</label>
            </div>
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="checkbox" id="inlineCheckbox12" disabled={isFiltering} onChange={() => check('ice')}/>
              <label className="form-check-label" for="inlineCheckbox12">Ice</label>
            </div>
          </div>
          <div className="form-check">
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="checkbox" id="inlineCheckbox13" disabled={isFiltering} onChange={() => check('normal')}/>
              <label className="form-check-label" for="inlineCheckbox13">Normal</label>
            </div>
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="checkbox" id="inlineCheckbox14" disabled={isFiltering} onChange={() => check('poison')}/>
              <label className="form-check-label" for="inlineCheckbox14">Poison</label>
            </div>
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="checkbox" id="inlineCheckbox15" disabled={isFiltering} onChange={() => check('psychic')}/>
              <label className="form-check-label" for="inlineCheckbox15">Psychic</label>
            </div>
          </div>
          <div className="form-check">
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="checkbox" id="inlineCheckbox16" disabled={isFiltering} onChange={() => check('rock')}/>
              <label className="form-check-label" for="inlineCheckbox16">Rock</label>
            </div>
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="checkbox" id="inlineCheckbox17" disabled={isFiltering} onChange={() => check('steel')}/>
              <label className="form-check-label" for="inlineCheckbox17">Steel</label>
            </div>
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="checkbox" id="inlineCheckbox18" disabled={isFiltering} onChange={() => check('water')}/>
              <label className="form-check-label" for="inlineCheckbox18">Water</label>
            </div>
          </div>
          <div className="form-check d-flex">
            <button className="btn btn-light" onClick={() => clearFilter()}>Clear</button>
            <button className={`btn btn-${isFiltering ? 'danger' : 'primary'}`} onClick={() => setFiltering(!isFiltering)}>{isFiltering ? 'Stop' : 'Filter'}</button>
          </div>
        </div>
      </div>
      {
        display && <div className='display-container'>
          <img src={display.sprites.other['official-artwork'].front_default} />
          <span className='text-center font-weight-bold'>{display.name.toUpperCase().replace('-', ' ')}</span>
        </div>
      }
    </div>
  )
}

export default Home;